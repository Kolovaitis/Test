import figure.Circle;
import figure.Figure;
import figure.Rectangle;

public class Test {
    @org.junit.jupiter.api.Test
    void test1() {
        final Figure figure = new Rectangle(55, 100);
        final double square = figure.getSquare();
        org.junit.jupiter.api.Assertions.assertEquals(5500, square);
    }

    @org.junit.jupiter.api.Test
    void test2() {
        final Figure figure = new Circle(150);
        final double square = figure.getSquare();
        org.junit.jupiter.api.Assertions.assertTrue(square > 100);
    }
}
